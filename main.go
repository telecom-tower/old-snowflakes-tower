package main

//go:generate esc -o html.go -prefix html html

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	log "github.com/Sirupsen/logrus"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811"
	"gitlab.com/telecom-tower/snowflakes"
)

const (
	columns           = 128
	rows              = 8
	defaultBrightness = 32
	gpioPin           = 18
)

func main() {
	var brightness = flag.Int(
		"brightness", defaultBrightness,
		"Brightness between 0 and 255.")
	var debug = flag.Bool("debug", false, "be verbose")

	iniflags.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting snowing tower")

	opt := ws2811.DefaultOptions
	opt.Brightness = *brightness
	opt.StripeType = ws2811.StripGRB
	opt.LedCount = rows * columns

	ws, err := ws2811.MakeWS2811(&opt)
	if err != nil {
		log.Panic(err)
	}
	err = ws.Init()
	if err != nil {
		log.Panic(err)
	}

	snowflakes.MakeSnow(ws, rows, columns)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Infoln("Main loop terminated!")

	ws.Wait()
	ws.Clear()
	ws.Render()
	ws.Wait()
	ws.Fini()

}
